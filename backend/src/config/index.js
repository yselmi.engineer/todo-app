module.exports = {
  LOG_LEVEL:  "info",
  HOST:  "0.0.0.0",
  PORT:  8080,
  DB:  "TodoDB",
};