const _ = require("lodash");
const { ObjectId } = require("mongodb");
const dbProvider = require("../../db");
const COLLECTION = "Todo";

exports.getTodos = async (conditions = {}) => {
  const db = await dbProvider.get();
  const todos = await db.collection(COLLECTION).find(conditions).toArray();
  return todos;
};

exports.getTodo = async (conditions = {}) => {
  const db = await dbProvider.get();
  const todo = await db.collection(COLLECTION).findOne(conditions);
  return todo;
};

exports.addTodo = async (data) => {
  const todo = _.cloneDeep(data);
  const db = await dbProvider.get();
  await db.collection(COLLECTION).insertOne(todo);
  return todo;
};

exports.updateTodo = async (data) => {
  const db = await dbProvider.get();
  const { value: todo } = await db
    .collection(COLLECTION)
    .findOneAndUpdate(
      { _id: ObjectId(data._id) },
      { $set: { ..._.omit(data, "_id") } },
      { returnOriginal: false }
    );
  return todo;
};

exports.deleteTodo = async (conditions = {}) => {
  const db = await dbProvider.get();
  await db.collection(COLLECTION).deleteOne(conditions);
};
