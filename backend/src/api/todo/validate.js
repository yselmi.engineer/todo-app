const _ = require("lodash");
const { ObjectId } = require("mongodb");
const dbProvider = require("../../db");

const COLLECTION = "Todo";

const Joi = require("joi");

const schema = Joi.object({
  title: Joi.string().min(2).required().messages({
    "string.base": `title should be a type of 'text'`,
    "string.empty": `title cannot be an empty field`,
    "string.min": `title should have a minimum length of {#limit}`,
    "any.required": `title is a required field`,
  }),
  description: Joi.string().min(2).required().messages({
    "string.base": `description should be a type of 'text'`,
    "string.empty": `description cannot be an empty field`,
    "string.min": `description should have a minimum length of {#limit}`,
    "any.required": `description is a required field`,
  }),
});

const todoExists = async (_id) => {
  if (!ObjectId.isValid(_id)) {
    return false;
  }
  const db = await dbProvider.get();
  return (
    (await db.collection(COLLECTION).countDocuments({ _id: ObjectId(_id) })) > 0
  );
};

const validatePayload = (payload) => {
  const { error: joiError } = schema.validate(payload);

  if (joiError) {
    const error = joiError.details[0];
    return {
      field: error.context.label,
      message: error.message,
    };
  }

  return false;
};

exports.exists = async (req, res, next) => {
  if (!(await todoExists(req.params._id))) {
    return res.status(404).send({ message: "item doesn't exists" });
  }

  return next();
};

exports.post = (req, res, next) => {
  const error = validatePayload(req.body);
  if (error) {
    return res.status(400).send(error);
  }

  return next();
};

exports.put = async (req, res, next) => {
  if (!(await todoExists(req.params._id))) {
    return res.status(404).send({ message: "item doesn't exists" });
  }

  const error = validatePayload(req.body);
  if (error) {
    return res.status(400).send(error);
  }

  return next();
};
