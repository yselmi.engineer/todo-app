const _ = require("lodash");
const { ObjectId } = require("mongodb");
const {
  getTodos,
  getTodo,
  addTodo,
  updateTodo,
  deleteTodo,
} = require("./service");

exports.list = async (req, res, next) => {
  const Todos = await getTodos();
  return res.json(Todos);
};

exports.item = async (req, res) => {
  const Todo = await getTodo({ _id: ObjectId(req.params._id) });
  return res.json(Todo);
};

exports.add = async (req, res) => {
  const Todo = await addTodo(req.body);
  return res.status(201).send(Todo);
};

exports.update = async (req, res) => {
  const Todo = await updateTodo({ _id: req.params._id, ...req.body });
  return res.status(200).send(Todo);
};

exports.delete = async (req, res) => {
  await deleteTodo({ _id: ObjectId(req.params._id) });
  return res.sendStatus(204);
};
