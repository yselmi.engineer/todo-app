const { MongoClient } = require("mongodb");
const _ = require("lodash");

const { DB } = require("../config");

let pool = undefined;

_.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

exports.connect = async (db) => {
  if (pool && pool.isConnected()) {
    return pool;
  }

  const dbUrl = "mongodb://localhost:27017/" + DB;
  const client = await MongoClient.connect(dbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  pool = client;

  return client;
};

exports.getPool = () => {
  return pool;
};

exports.close = async () => {
  await pool.close();
};

exports.get = async () => {
  const client = await exports.connect();
  return client.db();
};
