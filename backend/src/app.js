var express = require("express");
require("express-async-errors");
var cors = require("cors");

const http = require("http");
const config = require("./config");
const pino = require("pino");
const expressPino = require("express-pino-logger");
const bodyParser = require("body-parser");

var app = express();
const server = http.createServer(app);
const logger = pino({ level: config.LOG_LEVEL });

app
  .use(cors())
  .use(expressPino({ logger }))
  .use(express.json({ limit: "50mb" }))
  .use(bodyParser.json())
  .use("/status", require("./probe"))
  .use("/api/todos", require("./api/todo"))
  .use(require("./middleware/errorHandler"));

if (process.env.NODE_ENV !== "test") {
  server.listen(config.PORT, config.HOST, () => {
    logger.info(`Listening to ${config.HOST}:${config.PORT}`);
  });
}

module.exports = {
  app,
  server,
};
