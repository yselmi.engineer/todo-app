import { useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import { setFormCollapse, setInit } from "../redux/todo/action";

export default function TodosHeader() {
  const { collapse } = useSelector((state) => state.todo?.form);

  const dispatch = useDispatch();

  const onFormCollapserClick = useCallback(() => {
    if (collapse) {
      dispatch(setInit("form"));
    }
    dispatch(setFormCollapse(!collapse));
  }, [dispatch, collapse]);

  return (
    <Box sx={{ display: "flex", justifyContent: "flex-end", mb: 5, mt: 5 }}>
      <Button
        size="small"
        variant="contained"
        onClick={onFormCollapserClick}
        style={{ width: "30%" }}
      >
        {collapse ? "Cancel" : "Create"}
      </Button>
    </Box>
  );
}
