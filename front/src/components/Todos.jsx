import { useEffect, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";

import _ from "lodash";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Todo from "./Todo";
import {
  deleteTodo,
  getTodos,
  setForm,
  setFormCollapse,
  setFormEdition,
} from "../redux/todo/action";

export default function Todos() {
  const dispatch = useDispatch();

  const { data } = useSelector((state) => state.todo.items);

  useEffect(() => {
    dispatch(getTodos());
  }, [dispatch]);

  const onDeleteClick = useCallback(
    (todo) => {
      dispatch(deleteTodo(todo));
    },
    [dispatch]
  );

  const onEditClick = useCallback(
    (todo) => {
      dispatch(setForm(todo));
      dispatch(setFormEdition(true));
      dispatch(setFormCollapse(true));
    },
    [dispatch]
  );

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={2}>
        {_.map(data, (it) => (
          <Grid item xs={4}>
            <Todo
              data={it}
              onDeleteClick={onDeleteClick}
              onEditClick={onEditClick}
            />
          </Grid>
        ))}
      </Grid>
    </Box>
  );
}
