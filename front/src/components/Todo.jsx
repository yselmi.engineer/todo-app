import * as React from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";

export default function Todo({ data, onDeleteClick, onEditClick }) {
  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {data.title}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {data.description}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small" onClick={() => onDeleteClick(data)}>
          Delete
        </Button>
        <Button size="small" onClick={() => onEditClick(data)}>
          Edit
        </Button>
      </CardActions>
    </Card>
  );
}
