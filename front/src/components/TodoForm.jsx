import * as React from "react";
import _ from "lodash";
import { useSelector, useDispatch } from "react-redux";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import { Button } from "@mui/material";
import { createTodo, setForm, updateTodo } from "../redux/todo/action";

export default function TodoForm() {
  const { error, data, edition, collapse } = useSelector(
    (state) => state.todo.form
  );

  const dispatch = useDispatch();

  const submit = () => {
    if (edition) {
      dispatch(updateTodo({ id: data._id, payload: _.omit(data, "_id") }));
    } else {
      dispatch(createTodo(data));
    }
  };

  const onChange = (field) => (e) => {
    dispatch(
      setForm({
        ...data,
        [field]: e.target.value,
      })
    );
  };

  if (!collapse) {
    return <></>;
  }

  return (
    <Box
      component="form"
      sx={{
        width: "50%",
        "& .MuiTextField-root": { m: 1, width: "100%" },
      }}
      noValidate
      autoComplete="off"
    >
      <div>
        <TextField
          label="Title"
          required
          error={error?.field === "title"}
          helperText={error?.field === "title" && error?.message}
          variant="standard"
          value={data.title || ""}
          onChange={onChange("title")}
        />
      </div>
      <div>
        <TextField
          label="Description"
          required
          error={error?.field === "description"}
          helperText={error?.field === "description" && error?.message}
          multiline
          maxRows={4}
          variant="standard"
          value={data.description || ""}
          onChange={onChange("description")}
        />
      </div>
      <div>
        <Button
          size="small"
          variant="contained"
          onClick={submit}
          style={{ width: "30%", marginLeft: "70%" }}
        >
          Submit
        </Button>
      </div>
    </Box>
  );
}
