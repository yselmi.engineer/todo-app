import * as React from "react";
import { useDispatch, useSelector } from "react-redux";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";
import { setSnackbar } from "../redux/snackbar/action";

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export default function SimpleSnackbar() {
  const { open, message, type } = useSelector((state) => state.snackbar);

  const dispatch = useDispatch();

  const handleClose = React.useCallback(
    (event, reason) => {
      if (reason === "clickaway") {
        return;
      }

      dispatch(setSnackbar({ open: false }));
    },
    [dispatch]
  );

  return (
    <Snackbar
      anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
      open={open}
      autoHideDuration={3000}
      onClose={handleClose}
    >
      <Alert onClose={handleClose} severity={type} sx={{ width: "100%" }}>
        {message}
      </Alert>
    </Snackbar>
  );
}
