import _ from "lodash";
import { call, put } from "redux-saga/effects";
import { STATUS } from "../constants";
import { setSnackbar } from "./snackbar/action";

const sagaCrud =  (
  actionFunction,
  query,
  statusError = { setStatus: null, setError: null },
  snackbar = { success: false, error: false }
) =>
  function* ({ payload }) {
    const { setStatus, setError } = statusError;
    try {
      const { data } = yield call(query, payload);
      yield put(actionFunction.success({ payload, data }));
      if (snackbar.success) {
        yield put(
          setSnackbar({
            open: true,
            type: "success",
            message: "operation terminated successfully",
          })
        );
      }
      if (setStatus) {
        yield put(setStatus(STATUS.OK));
      }
      if (setError) {
        yield put(setError(undefined));
      }
    } catch (error) {
      if (snackbar.error) {
        yield put(
          setSnackbar({
            open: true,
            type: "error",
            message: _.get(error, 'response.data.message')
          })
        );
      }

      console.log("error", error.message);
      if (setError) {
        yield put(setError(error));
      }

      if (setStatus) {
        yield put(setStatus(STATUS.ERROR));
      }

      yield put(actionFunction.failure(error));
    }
  };

  export default sagaCrud;
