import { handleActions } from "redux-actions";
import { setSnackbar } from "./action";

const DEFAULT_STATE = {
  open: false,
  message: "",
  type: "success",
};

const handlers = {
  [setSnackbar]: (state, action) => ({
    ...state,
    ...action.payload,
  }),
};

export default handleActions(handlers, DEFAULT_STATE);
