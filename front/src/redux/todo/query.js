import api from "../api";

const API = "http://localhost:8080/api/todos";

export const getTodosQuery = async (params) => {
  return await api.get(API, params);
};

export const createTodoQuery = async (payload) => {
  return await api.post(API, payload);
};

export const updateTodoQuery = async ({ id, payload }) => {
  return await api.put(`${API}/${id}`, payload);
};

export const deleteTodoQuery = async (payload) => {
  return await api.delete(`${API}/${payload._id}`);
};
