import { createAction } from "redux-actions";
import { createRoutine } from "redux-saga-routines";

export const setStatus = createAction("SET_TODO_STATUS");
export const setError = createAction("SET_TODO_ERROR");
export const setInit = createAction("SET_TODO_INIT");

export const setForm = createAction("SET_TODO_FORM");

export const getTodos = createRoutine("GET_TODOS");
export const getTodo = createRoutine("GET_TODO");
export const createTodo = createRoutine("CREATE_TODO");
export const deleteTodo = createRoutine("DELETE_TODO");
export const updateTodo = createRoutine("UPDATE_TODO");

export const setFormEdition = createAction("SET_FORM_EDITION");
export const setFormCollapse = createAction("SET_FORM_COLLAPSE");
