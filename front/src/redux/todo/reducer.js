import _ from "lodash";
import { handleActions } from "redux-actions";
import {
  setStatus,
  setError,
  setInit,
  getTodos,
  setForm,
  createTodo,
  deleteTodo,
  setFormEdition,
  setFormCollapse,
  updateTodo,
} from "./action";

import { STATUS } from "../../constants";

const DEFAULT_STATE = {
  items: {
    data: [],
    status: STATUS.OK,
  },
  form: {
    status: STATUS.OK,
    error: undefined,
    collapseForm: false,
    edition: false,
    data: {},
  },
};

const handlers = {
  [setInit]: (state, action) => ({
    ...state,
    [action.payload]: DEFAULT_STATE[action.payload],
  }),
  [setStatus]: (state, action) => ({
    ...state,
    [action.payload.key]: {
      ...state[action.payload.key],
      status: action.payload.status,
    },
  }),
  [setError]: (state, action) => ({
    ...state,
    [action.payload.key]: {
      ...state[action.payload.key],
      error: {
        ...action?.payload?.error?.response?.data,
        status: action?.payload?.error?.response?.status,
      },
    },
  }),
  [getTodos.TRIGGER]: (state, action) => {
    return {
      ...state,
      items: {
        ...state.items,
        status: STATUS.FETCHING,
      },
    };
  },
  [getTodos.SUCCESS]: (state, action) => {
    return {
      ...state,
      items: {
        ...state.items,
        data: action.payload.data,
        status: STATUS.SUCCESS,
      },
    };
  },
  [createTodo.TRIGGER]: (state, action) => ({
    ...state,
    form: { ...state.form, status: STATUS.CREATING },
  }),

  [createTodo.SUCCESS]: (state, action) => ({
    ...state,
    items: {
      ...state.items,
      data: [...state.items?.data, action.payload?.data],
    },
  }),
  [createTodo.TRIGGER]: (state, action) => ({
    ...state,
    form: { ...state.form, status: STATUS.UPDATING },
  }),
  [updateTodo.SUCCESS]: (state, action) => {
    const data = _.chain(state).get("items.data").cloneDeep().value();
    _.chain(data)
      .find({ _id: action.payload?.data?._id })
      .merge(action.payload.data)
      .value();
    return {
      ...state,
      items: {
        ...state.items,
        data,
      },
    };
  },
  [deleteTodo.SUCCESS]: (state, action) => ({
    ...state,
    items: {
      ...state.items,
      data: _.filter(
        state.items?.data,
        (it) => it._id !== action.payload?.payload?._id
      ),
    },
  }),
  [setForm]: (state, action) => ({
    ...state,
    form: {
      ...state.form,
      data: action.payload,
    },
  }),
  [setFormEdition]: (state, action) => ({
    ...state,
    form: {
      ...state.form,
      edition: action.payload,
    },
  }),
  [setFormCollapse]: (state, action) => ({
    ...state,
    form: {
      ...state.form,
      collapse: action.payload,
    },
  }),
};

export default handleActions(handlers, DEFAULT_STATE);
