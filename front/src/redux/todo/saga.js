import _ from "lodash";
import { takeLatest, all } from "redux-saga/effects";
import sagaCrud from "../saga-crud";
import {
  setStatus,
  setError,
  getTodos,
  deleteTodo,
  createTodo,
  updateTodo,
} from "./action";
import {
  createTodoQuery,
  deleteTodoQuery,
  getTodosQuery,
  updateTodoQuery,
} from "./query";

const statusErrorNodeInterceptor = (key) => ({
  setStatus: (status) => setStatus({ key, status }),
  setError: (error) => setError({ key, error }),
});

export default function* managerSaga() {
  yield all([
    takeLatest(
      getTodos.TRIGGER,
      sagaCrud(getTodos, getTodosQuery, statusErrorNodeInterceptor("items"))
    ),
    takeLatest(
      deleteTodo.TRIGGER,
      sagaCrud(
        deleteTodo,
        deleteTodoQuery,
        statusErrorNodeInterceptor("form"),
        { success: true, error: true }
      )
    ),
    takeLatest(
      createTodo.TRIGGER,
      sagaCrud(
        createTodo,
        createTodoQuery,
        statusErrorNodeInterceptor("form"),
        { success: true, error: false }
      )
    ),
    takeLatest(
      updateTodo.TRIGGER,
      sagaCrud(
        updateTodo,
        updateTodoQuery,
        statusErrorNodeInterceptor("form"),
        { success: true, error: false }
      )
    ),
  ]);
}
