import { combineReducers } from "redux";
import todoReducer from "./todo/reducer";
import snackbarReducer from "./snackbar/reducer";

export default combineReducers({
  todo: todoReducer,
  snackbar: snackbarReducer,
});
