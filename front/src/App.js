import "./App.css";
import TodoForm from "./components/TodoForm";
import Todos from "./components/Todos";
import TodosHeader from "./components/TodosHeader";
import Snackbar from "./components/Snackbar";

function App() {
  return (
    <div className="app-container">
      <TodoForm />
      <TodosHeader />
      <Todos />
      <Snackbar />
    </div>
  );
}

export default App;
